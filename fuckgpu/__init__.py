import logging
import numpy as np
import theano


class NullOp(theano.Op):

    check_input = False

    def __eq__(self, other):
        return type(self) == type(other)

    def __hash__(self):
        return hash(type(self))

    def __str__(self):
        return 'NullOp'

    def make_node(self, x):
        raise NotImplementedError()

    def perform(self, node, inputs, output_storage):
        x, = inputs
        z, = output_storage
        z[0] = np.asarray(x)

    def grad(self, inputs, grads):
        gz, = grads
        return gz

    def R_op(self, inputs, eval_points):
        ev, = eval_points
        return [self(ev)]

    def infer_shape(self, node, xshp):
        return xshp


class FuckGPUConstructorFn(object):

    def __init__(self, fn):
        self.fn = fn

    def __call__(self, maker, input_storage, inputs_data):
        queue = list(maker.fgraph.outputs)

        while queue:
            node = queue.pop()

            if hasattr(node, 'owner'):
                if node.owner:
                    node.owner.op._op_use_c_code = False

                    if type(node.owner.op).__module__.startswith(
                            'theano.sandbox.cuda') and \
                        type(node.owner.op).__name__ in (
                            'HostFromGpu', 'GpuFromHost'):
                        assert len(node.owner.inputs) == 1

                        node.owner.op = NullOp()

            for parent in node.get_parents():
                queue.append(parent)

        if hasattr(theano.sandbox, 'cuda'):
            logging.warning('CUDA enabled; disabling.')

            delattr(theano.sandbox, 'cuda')

        theano.config.cxx = False

        # Override make_thunk.
        old_make_thunk_fn = theano.gof.op.Op.make_thunk

        def make_thunk_fn(self, *args, **kwargs):
            self._op_use_c_code = False

            return old_make_thunk_fn(self, *args, **kwargs)

        theano.gof.op.Op.make_thunk = make_thunk_fn

        return self.fn(maker, input_storage, inputs_data)


def fuck():
    theano.config.experimental.unpickle_gpu_on_cpu = True

    theano.compile.function_module._constructor_Function = \
        FuckGPUConstructorFn(
            theano.compile.function_module._constructor_Function)
