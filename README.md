FuckGPU
-------

FuckGPU is a small library which removes pesky hard-coded GPU operations (e.g. HostFromGpu, GpuFromHost) in pickled Theano functions transparently.

Usage example:

	import cPickle as pickle
	import fuckgpu

	if __name__ == "__main__":
		fuckgpu.fuck()

		f = open('pickled_theano_function.pkl', 'rb')
		fn = pickle.load(f)  # No crazy Exceptions are being thrown an the function can simply be used!
